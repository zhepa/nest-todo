FROM node:19.5-alpine AS build
WORKDIR /opt/app
COPY . .
RUN yarn && yarn build

FROM node:19.5-alpine AS app
WORKDIR /opt/app
COPY package*.json ./
COPY yarn.lock ./yarn.lock
RUN yarn --production --frozen-lockfile && yarn cache clean
COPY --from=build /opt/app/dist ./dist
EXPOSE 3000
CMD ["node", "dist/main"]

