import { Module } from '@nestjs/common';
import { TodoService } from 'modules/todo/todo.service';
import { TodoController } from 'modules/todo/todo.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TodoSchema } from 'modules/todo/schemas/todo.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Todo',
        schema: TodoSchema
      }
    ])
  ],
  controllers: [TodoController],
  providers: [TodoService],
  exports: [TodoService]
})
export class TodoModule {}
