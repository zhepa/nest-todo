import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../../user/schemas/user.schema';


@Schema()
export class Todo extends mongoose.Document {
  @Prop()
  title: string;
  
  @Prop()
  description: string;
  
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  })
  owner: User;
}


export type TodoDocument = mongoose.HydratedDocument<Todo>;
export const TodoSchema = SchemaFactory.createForClass(Todo);
