import { Exclude } from 'class-transformer';
import { ObjectIdProperty } from 'decorators/objectid-property.decorator';

@Exclude()
export class TodoOperationResultModel {
  @ObjectIdProperty()
  id: string;
}
