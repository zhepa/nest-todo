import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ObjectIdProperty } from 'decorators/objectid-property.decorator';


@Exclude()
export class TodoModel {
  @ObjectIdProperty()
  id: string;
  
  @Expose()
  @ApiProperty({
    example: 'Домашка по NestJS'
  })
  title: string;
  
  @Expose()
  @ApiPropertyOptional({
    example: 'Сделай странную REST (не совсем) апишку'
  })
  description: string;
}
