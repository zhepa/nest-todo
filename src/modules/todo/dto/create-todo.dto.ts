import { Exclude, Expose } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

@Exclude()
export class CreateTodoDto {
  @Expose()
  @IsString({
    message: 'Заголовок должен быть строкой'
  })
  @IsNotEmpty({
    message: 'Нужно указать заголовок задачи'
  })
  @ApiProperty({
    example: 'Домашка по NestJS'
  })
  readonly title: string;
  
  @Expose()
  @IsString({
    message: 'Описание должно быть строкой'
  })
  @IsOptional()
  @ApiPropertyOptional({
    example: 'Сделай странную REST (не совсем) апишку'
  })
  readonly description: string;
}
