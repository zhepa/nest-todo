import { Exclude } from 'class-transformer';
import { ObjectIdProperty } from 'decorators/objectid-property.decorator';

@Exclude()
export class DeleteTodoDto {
  @ObjectIdProperty()
  readonly id: string;
}
