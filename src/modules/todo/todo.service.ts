import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Todo, TodoDocument } from './schemas/todo.schema';


interface CreateTodoParams {
  title: string,
  description?: string,
  owner: Types.ObjectId
}

@Injectable()
export class TodoService {
  constructor(
    @InjectModel(Todo.name) private todoModel: Model<TodoDocument>
  ) {}
  
  async create({ title, description, owner }: CreateTodoParams): Promise<TodoDocument> {
    const task = new this.todoModel({
      title,
      description,
      owner
    });
    
    return task.save();
  }
  
  async findById(id: Types.ObjectId): Promise<TodoDocument> {
    return this.todoModel.findById(id).exec();
  }
  
  async findByOwner(ownerId: Types.ObjectId): Promise<TodoDocument[]> {
    return this.todoModel.find({
      owner: ownerId
    }).exec();
  }
  
  async delete(id: Types.ObjectId): Promise<void> {
    await this.todoModel.deleteOne({
      _id: id
    });
  }
}
