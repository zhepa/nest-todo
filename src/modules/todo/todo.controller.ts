import { Body, Controller, Delete, Get, HttpException, HttpStatus, Post, Request, UseGuards } from '@nestjs/common';
import { Types } from 'mongoose';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToInstance } from 'class-transformer';
import { TodoService } from './todo.service';
import { AuthGuard } from 'guards/auth.guard';
import { DeleteTodoDto, CreateTodoDto } from './dto';
import { TodoModel, TodoOperationResultModel } from './models';


@ApiTags('Задачи')
@UseGuards(AuthGuard)
@Controller('/')
export class TodoController {
  constructor(private todoService: TodoService) {}
  
  @Post('/create')
  @ApiResponse({
    status: 200,
    type: TodoOperationResultModel
  })
  @ApiOperation({ summary: 'Создание задачи' })
  async create(@Body() body: CreateTodoDto, @Request() req) {
    const { title, description } = body;
    const task = await this.todoService.create({
      title,
      description,
      owner: req.user.id
    });
    
    return plainToInstance(TodoOperationResultModel, task);
  }
  
  @Get('/get')
  @ApiResponse({
    status: 200,
    type: TodoModel,
    isArray: true,
  })
  @ApiOperation({ summary: 'Список задач' })
  async findMany(@Request() req) {
    const tasks = await this.todoService.findByOwner(req.user.id);
    
    return tasks.map(task => plainToInstance(TodoModel, task));
  }
  
  @Delete('/delete')
  @ApiResponse({
    status: 200,
    type: TodoOperationResultModel,
  })
  @ApiOperation({ summary: 'Удаление задачи' })
  async delete(@Body() body: DeleteTodoDto, @Request() req) {
    const id = new Types.ObjectId(body.id);
    const task = await this.todoService.findById(id);
    
    if (!task) {
      throw new HttpException('Задача не найдена', HttpStatus.NOT_FOUND);
    }
    
    if (!task.owner.equals(req.user.id)) {
      throw new HttpException('Ошибка доступа', HttpStatus.FORBIDDEN);
    }
    
    await this.todoService.delete(id);
    
    return plainToInstance(TodoOperationResultModel, task);
  }
}
