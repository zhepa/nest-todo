import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { Model, Types } from 'mongoose';
import { hashPassword } from 'utils/crypto';


@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>
  ) {}
  
  async create({ password }: { password: string }): Promise<UserDocument> {
    const user = new this.userModel({
      password: await hashPassword(password)
    });
    
    return user.save();
  }

  async findById(id: Types.ObjectId): Promise<UserDocument> {
    return this.userModel.findById(id).exec();
  }
}
