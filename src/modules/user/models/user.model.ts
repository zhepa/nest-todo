import { Exclude } from 'class-transformer';
import { ObjectIdProperty } from 'decorators/objectid-property.decorator';

@Exclude()
export class UserModel {
  @ObjectIdProperty()
  id: string;
}
