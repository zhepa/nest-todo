import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { plainToInstance } from 'class-transformer';
import { AuthGuard } from 'guards/auth.guard';
import { UserModel } from './models';
import { UserService } from 'modules/user/user.service';


@ApiTags('Профиль')
@UseGuards(AuthGuard)
@Controller('/')
export class UserController {
  constructor(private userService: UserService) {}
  
  @Get('/user')
  @ApiResponse({
    status: 200,
    type: UserModel
  })
  @ApiOperation({ summary: 'Профиль текущего пользователя' })
  async getProfile(@Request() req) {
    const user = await this.userService.findById(req.user.id);
    
    return plainToInstance(UserModel, user);
  }
}
