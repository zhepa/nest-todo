import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from 'modules/user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy, LocalStrategy } from 'modules/auth/strategies';
import { JwtModule } from '@nestjs/jwt';
import config from 'utils/config';
import { AuthController } from 'modules/auth/auth.controller';


@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: config.jwtSecret,
      signOptions: {
        expiresIn: '30d'
      }
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule {}
