import { Injectable } from '@nestjs/common';
import { UserService } from 'modules/user/user.service';
import { Types } from 'mongoose';
import { comparePassword } from 'utils/crypto';
import { UserDocument } from 'modules/user/schemas/user.schema';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService
  ) {}
  
  async login(id: Types.ObjectId, password: string): Promise<any> {
    const user = await this.usersService.findById(id);

    if (!user || !await comparePassword(password, user.password)) {
      return null;
    }
    
    delete user.password;
    
    return user;
  }
  
  async createToken(user: UserDocument) {
    return this.jwtService.sign({
      id: user.id,
    });
  }
}
