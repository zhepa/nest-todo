import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SessionModel } from './models';
import { SigninDto, SignupDto } from './dto';
import { plainToInstance } from 'class-transformer';
import { Types } from 'mongoose';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'modules/auth/auth.service';


@ApiTags('Авторизация')
@Controller('/')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}
  
  @Post('/signup')
  @ApiResponse({
    status: 200,
    type: SessionModel
  })
  @ApiOperation({ summary: 'Создание аккаунта' })
  async signUp(@Body() body: SignupDto) {
    const { password } = body;
    const user = await this.userService.create({ password });
    const token = await this.authService.createToken(user);
  
    return plainToInstance(SessionModel, {
      id: user.id,
      token
    });
  }
  
  @Post('/signin')
  @ApiResponse({
    status: 200,
    type: SessionModel
  })
  @UseGuards(AuthGuard('local'))
  @ApiOperation({ summary: 'Авторизация' })
  async signIn(@Body() body: SigninDto) {
    const user = await this.userService.findById(new Types.ObjectId(body.id));
    const token = await this.authService.createToken(user);
    
    return plainToInstance(SessionModel, {
      id: user.id,
      token
    });
  }
}
