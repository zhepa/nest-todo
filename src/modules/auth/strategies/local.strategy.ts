import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { Types } from 'mongoose';


@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'id'
    });
  }
  
  async validate(id: string, password: string): Promise<any> {
    const user = await this.authService.login(new Types.ObjectId(id), password);
    
    if (!user) {
      throw new HttpException('Неверный логин или пароль', HttpStatus.UNAUTHORIZED);
    }
    
    return user;
  }
}
