import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectIdProperty } from 'decorators/objectid-property.decorator';
import { IsString } from 'class-validator';


@Exclude()
export class SigninDto {
  @ObjectIdProperty({
    description: 'Логин'
  })
  readonly id: string;
  
  @Expose()
  @IsString()
  @ApiProperty({
    description: 'Пароль'
  })
  readonly password: string;
}
