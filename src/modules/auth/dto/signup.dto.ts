import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';


@Exclude()
export class SignupDto {
  @Expose()
  @IsString()
  @MinLength(6, {
    message: 'Пароль должен сосоять минимум из 6 символов'
  })
  @ApiProperty({
    description: 'Пароль'
  })
  readonly password: string;
}
