import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { RedocModule } from '@nicholas.braun/nestjs-redoc';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';


(async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    whitelist: true
  }));
  
  const config = new DocumentBuilder()
    .setTitle('API Reference')
    .setVersion('1.0')
    .build();
  
  const document = SwaggerModule.createDocument(app, config);
  
  await RedocModule.setup('docs', app, document, {
    title: 'API Reference',
    sortPropsAlphabetically: false,
    hideDownloadButton: false,
    hideHostname: false,
  });
  
  await app.init();
  await app.listen(3000);
}());
