import { applyDecorators } from '@nestjs/common';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { Types } from 'mongoose';
import { IsString } from 'class-validator';

export function ObjectIdProperty(options?: { optional?: boolean, description?: string }) {
  const BaseApiProperty = options?.optional ? ApiPropertyOptional : ApiProperty;
  
  return applyDecorators(
    Expose(),
    IsString(),
    BaseApiProperty({
      example: new Types.ObjectId(),
      description: options?.description || 'Идентификатор'
    })
  );
}
