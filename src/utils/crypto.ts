import * as bcrypt from 'bcrypt';


export const hashPassword = (password: string) =>
  bcrypt.hash(password, 10);

export const comparePassword = (plainPassword: string, hashedPassword: string) =>
  bcrypt.compare(plainPassword, hashedPassword);
