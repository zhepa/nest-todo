import * as dotenv from 'dotenv';
import { resolve } from 'path';

dotenv.config({
  path: resolve(process.cwd() + '/.env')
});

const mongodb = {
  username: process.env.MONGODB_USERNAME,
  password: process.env.MONGODB_PASSWORD,
  database: process.env.MONGODB_DATABASE,
  hostname: process.env.MONGODB_HOSTNAME || '0.0.0.0',
  port: +process.env.MONGODB_PORT || 27017
};

export default {
  jwtSecret: process.env.JWT_SECRET || 'changeme',
  mongodb: {
    ... mongodb,
    connectionString: `mongodb://${mongodb.username}:${mongodb.password}@${mongodb.hostname}:${mongodb.port}/${mongodb.database}?authSource=admin`
  }
};

